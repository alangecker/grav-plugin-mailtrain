<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Twig\Twig;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class MaintrainPlugin
 * @package Grav\Plugin
 */
class MailtrainPlugin extends Plugin
{
    public static function getSubscribedEvents()
    {
        return [
            'onFormProcessed' => ['onFormProcessed', 0]
        ];
    }
    public function onFormProcessed(Event $event)
    {
        $form = $event['form'];
        $action = $event['action'];
        $params = $event['params'];
        
        // Prepare Twig variables
        $vars = array(
            'form' => $form
        );
        try {
            switch ($action) {
                case 'mailtrain_subscribe':
                    $this->subscribe($params, $vars);
            }
        } catch (\RuntimeException $e) {
            $this->grav->fireEvent('onFormValidationError', new Event([
                'form'    => $form,
                'message' => $e->getMessage()
            ]));
            $event->stopPropagation();
        }
    }
    private function subscribe($params, $vars) {
        $twig = $this->grav['twig'];
        

        // validation
        if (!isset($params['host']) || !$params['host']) {
            throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.PLEASE_CONFIGURE_A_HOST'));
        }
        if (!preg_match('/^(http|https):\/\//',$params['host'])) {
            throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.HOST_INVALID'));
        }
        if (!isset($params['list_id']) || !$params['list_id']) {
            throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.PLEASE_CONFIGURE_A_LIST_ID'));
        }
        if (!isset($params['access_token']) || !$params['access_token']) {
            $tokens = $this->config->get('security.mailtrain_tokens');
            if(!isset($tokens[$params['host']])) {
                throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.PLEASE_CONFIGURE_A_ACCESS_TOKEN'));
            }
            $params['access_token'] = $tokens[$params['host']];
        }
        if (!isset($params['email']) || !$params['email']) {
            throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.PLEASE_PROVIDE_A_MAIL'));
        }
        if(!isset($params['require_confirmation']) || $params['require_confirmation'] != FALSE) {
            $params['require_confirmation'] = true;
        }


        // post data
        $data = [
            'EMAIL' => $twig->processString($params['email'], $vars),
            'REQUIRE_CONFIRMATION' => $params['require_confirmation'] ? '1' : '0'
        ];
        if(isset($params['first_name'])) $data['FIRST_NAME'] = $twig->processString($params['first_name'], $vars);
        if(isset($params['last_name'])) $data['LAST_NAME'] = $twig->processString($params['last_name'], $vars);

        if(isset($params['merge'])) {
            if(!is_array($params['merge'])) {
                throw new \RuntimeException($this->grav['language']->translate('PLUGIN_MAILTRAIN.INVALID_MERGE_OPTION'));
            }
            foreach($params['merge'] as $key => $value) {
                $data['MERGE_'.strtoupper($key)] = $twig->processString($value, $vars);
            }
        }

        // send api call
        $url = $params['host'] . "/api/subscribe/" . $params['list_id'] . "?access_token=" . $params['access_token'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec ($ch));
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);

        // handle response
        if($result && isset($result->error)) {
            throw new \RuntimeException($result->error);
        } else if($statusCode == 200) {
            // success
            return;
        } else {
            var_dump($statusCode);
            var_dump($result);
            throw new \RuntimeException('unknown mailtrain error');
        }
    }
}