# Grav Mailtrain Plugin

The **mailtrain plugin** for [Grav](http://github.com/getgrav/grav) adds the ability to add subscribers to mailtrain lists.

# Installation

```
$ cd user/plugins
$ git clone https://gitlab.com/alangecker/grav-plugin-mailtrain.git mailtrain
```

<!-- The mailtrain plugin is easy to install with GPM.

```
$ bin/gpm install mailtrain
``` -->

# Configuration

Only the access tokens are configured globally in the `user/config/security.yaml` file. 


```yaml
mailtrain_tokens:
  "https://mailtrain.example.com": 5f7d62a09c[...]
```

don't forget to add this file to `.gitignore`!

# Usage with Forms

```yaml
---
title: Newsletter
form:
    name: newsletter
    fields:
        -
            name: email
            type: email
            label: Email
            placeholder: 'Enter your email address'
            validate:
                rule: email
                required: true
        - 
            name: gpgkey
            type: textarea
            label: Your GPG Public Key
            placeholder: starts with '-----BEGIN PGP PUBLIC KEY BLOCK-----'
    buttons:
        -
            type: submit
            value: subscribe

    process:
        - mailtrain_subscribe:
            host: https://mailtrain.example.com
            list_id: B1IxjB9Bf
            require_confirmation: true
            email: '{{ form.value.email }}'
            merge:
                gpg: '{{ form.value.gpg }}'
        -
            message: 'Thank you for your subscription!'
---

Subscribe here to our newsletter
```
